﻿open System.IO
open Argu
open MongoDB.Bson
open MongoDB.Bson.Serialization.Attributes
open MongoDB.Driver
open NReco.VideoInfo

type CliOptions =
    | [<Mandatory; AltCommandLine("-m")>] MongoUrl of string
    | [<Mandatory; AltCommandLine("-d")>] MongoDatabaseName of string
    | [<Mandatory; AltCommandLine("-c")>] MongoCollectionName of string
    | [<Mandatory; AltCommandLine("-v")>] VideoDir of string
with
    interface IArgParserTemplate with
        member s.Usage =
            match s with
            | MongoUrl _ -> "MongoDB URL."
            | MongoDatabaseName _ -> "MongoDB database name."
            | MongoCollectionName _ -> "MongoDB collection name."
            | VideoDir _ -> "Directory containing video files."

type Document = { 
    [<BsonId>] Id: BsonObjectId;
    [<BsonElement("name")>] Name: string;
    [<BsonElement("filename")>] Filename: string;
    [<BsonElement("duration_secs")>] DurationSecs: int }

let getDurationSecs (file: string) =
    let probe = FFProbe()
    try
        let info = probe.GetMediaInfo file
        let len = (int)info.Duration.TotalSeconds
        // sometimes the video length is wildly off for some reason, so just guess 1 minute (most videos are longer)
        if len > 300 then 
            printfn "%s: length was wildly too high - assuming 1 minute" file
            Some 60
        else Some len
    with ex -> 
        printfn "%s: can't read length - skipping" file
        None

let run (opts: ParseResults<CliOptions>) =
    printfn "connecting to database..."
    let client = MongoClient(opts.GetResult<@ MongoUrl @>)
    let db = client.GetDatabase (opts.GetResult<@ MongoDatabaseName @>)
    let colName = opts.GetResult<@ MongoCollectionName @>
    let videoDir = opts.GetResult<@ VideoDir @>
    printfn "clearing old collection..."
    db.DropCollection colName // clear any old documents in the collection
    let collection = db.GetCollection<Document> colName
    let videoFiles = Directory.GetFiles videoDir |> List.ofArray
    printfn "adding %d files to collection..." (List.length videoFiles)
    let videoName x =
        let x = Path.GetFileNameWithoutExtension x
        if x.EndsWith(" Opening") then x.Substring(0, x.Length - " Opening".Length) else x
    let toDocument x = 
        match getDurationSecs x with
        | Some time -> Some {
            Id = BsonObjectId(ObjectId.GenerateNewId());
            Name = videoName x;
            Filename = Path.GetFileName x;
            DurationSecs = time }
        | None -> None
    let documents = videoFiles |> List.map toDocument
    documents |> List.choose id |> collection.InsertMany
    printfn "done!"
    
[<EntryPoint>]
let main argv =
    let parser = ArgumentParser.Create<CliOptions>(errorHandler = new ProcessExiter())
    let results = parser.Parse argv
    run results
    0