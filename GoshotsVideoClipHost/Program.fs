﻿open System
open System.IO
open System.Linq
open System.Web
open Argu
open NReco.VideoConverter
open Suave
open Suave.Filters
open Suave.Operators
open Suave.Sockets
open Suave.Writers

let port = 8119us

type CliOptions =
    | [<MainCommand; Last>] VideoDir of string
with
    interface IArgParserTemplate with
        member s.Usage =
            match s with
            | VideoDir _ -> "Directory containing video files."

let respondWithVideoBytes (bytes: byte[]) =
    bytes |> Successful.ok >=> setMimeType "video/mp4" >=> setHeader "Accept-Ranges" "none"

let serveVideoRaw (path: string) =
    printfn "serving '%s' raw" path
    File.ReadAllBytes path |> respondWithVideoBytes 

let serveVideoPart (path: string) (start: int option) (len: int option) =
    let intOptToStr o = match o with
                        | Some x -> sprintf "%d" x
                        | None -> "?"
    let intOptToNullableFloat o = match o with
                                  | Some x -> System.Nullable<float32>(float32(x))
                                  | None -> System.Nullable<float32>()
    let inputFormat = null // auto-detect
    let seek = intOptToNullableFloat start
    let duration = intOptToNullableFloat len
    let outArgs = "-preset fast -movflags faststart"
    let convertSettings = ConvertSettings(Seek = seek, MaxDuration = duration, CustomOutputArgs = outArgs)
    let outputFormat = Format.mp4
    let converter = FFMpegConverter()
    use memStream = new MemoryStream()
    printfn "serving '%s' (%s - %s)" path (intOptToStr start) (intOptToStr len)
    converter.ConvertMedia(path, inputFormat, memStream, outputFormat, convertSettings)
    memStream.ToArray() |> respondWithVideoBytes

let serveVideo (path: string) (start: int option) (len: int option) =
    match start, len, Path.GetExtension(path).ToLower() with
    | None, None, ".mp4" -> serveVideoRaw path
    | _ -> serveVideoPart path start len

let serveFile (videoDir: string) (r: HttpRequest) =
    let localPath = r.url.LocalPath |> HttpUtility.UrlDecode |> Path.GetFileName |> fun x -> Path.Combine(videoDir, x)
    if File.Exists localPath then
        let queryStringToIntOption name = match r.queryParam name with
                                          | Choice1Of2 value -> Some (Int32.Parse value)
                                          | Choice2Of2 err -> None
        let startArg = queryStringToIntOption "start"
        let lenArg = queryStringToIntOption "len"
        serveVideo localPath startArg lenArg
    else
        RequestErrors.NOT_FOUND <| "Video not found."

let run (opts: ParseResults<CliOptions>) =
    let videoDir = opts.GetResult(<@ VideoDir @>, defaultValue = ".")
    printfn "starting video clip server in '%s'" videoDir
    let app = choose [ GET >=> request (serveFile videoDir) ]
    let binding = { ip = System.Net.IPAddress.Parse("127.0.0.1"); port = port }
    startWebServer { defaultConfig with bindings = [ { scheme = HTTP; socketBinding = binding } ] } app

[<EntryPoint>]
let main argv =
    let parser = ArgumentParser.Create<CliOptions>(errorHandler = new ProcessExiter())
    let results = parser.Parse argv
    run results
    0
